package gui.Forms.reglib;

import javax.swing.*;

public class OrderWatchOne {
    private JTextField textField1;
    public JPanel mainPanel;
    private JLabel label;
    int book_id;
    int out;

    public int getOut() {
        return out;
    }

    public void setOut(int out) {
        this.out = out;
    }

    public int getBook_id() {
        return book_id;
    }

    public void setBook_id(int book_id) {
        this.book_id = book_id;
    }

    public JTextField getTextField1() {
        return textField1;
    }

    public void setText(String text){
        label.setText(text);
    }
}
