package gui.Forms.reglib;

import javax.swing.*;

public class RegLibMainForm {
    public void setMainLabel(String text) {
        mainLabel.setText(text);
    }
    public void setrole(String text) {
        role.setText(text);
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public JButton getTakeBiletB() {
        return takeBiletB;
    }

    public JButton getContBilB() {
        return contBilB;
    }

    public void setOutpost(String text) {
       outpost.setText(text);
    }

    public JButton getNewReaderB() {
        return newReaderB;
    }

    public JButton getExitB() {
        return exitB;
    }

    public JButton getTakeBookB() {
        return takeBookB;
    }

    public JButton getWatchOrders() {
        return watchOrders;
    }

    public JLabel getMainLabel() {
        return mainLabel;
    }

    public JLabel getWatermarkLabel() {
        return watermarkLabel;
    }

    private JPanel panel1;
    private JButton takeBiletB;
    private JButton contBilB;
    private JButton newReaderB;
    private JButton exitB;
    private JButton takeBookB;
    private JButton watchOrders;
    private JLabel mainLabel;
    private JLabel watermarkLabel;
    private JLabel role;
    private JLabel outpost;
}
