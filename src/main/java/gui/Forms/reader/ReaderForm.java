package gui.Forms.reader;

import javax.swing.*;

public class ReaderForm {
    private JPanel panel;
    private JButton exitB;
    private JButton create_order;
    private JButton viewOrder;
    private JButton посмотертьПрофильButton;
    private JLabel hello;
    private JLabel role;

    public void setHello(String text) {
        hello.setText(text);
    }
    public void setrole(String text) {
        role.setText(text);
    }

    public JPanel getPanel() {
        return panel;
    }

    public JButton getExitB() {
        return exitB;
    }

    public JButton getCreate_order() {
        return create_order;
    }

    public JButton getViewOrder() {
        return viewOrder;
    }

    public JButton getПосмотертьПрофильButton() {
        return посмотертьПрофильButton;
    }

    public JLabel getHello() {
        return hello;
    }

    public JLabel getRole() {
        return role;
    }
}
