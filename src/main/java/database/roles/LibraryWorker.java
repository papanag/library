package database.roles;

public class LibraryWorker {
    private String name;
    private Integer outpost_id;

    public LibraryWorker(String name, int outpost_id) {
        this.name = name;
        this.outpost_id = outpost_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOutpost_id() {
        return outpost_id;
    }

    public void setOutpost_id(Integer outpost_id) {
        this.outpost_id = outpost_id;
    }
}
