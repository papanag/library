package database;

import java.sql.*;

public class BaseConnection {
    // JDBC URL, username and password of MySQL server
    private static final String url = "jdbc:oracle:thin:@localhost:1521:XE";
    public static  String user = "";
    public static  String password = "";

    // JDBC variables for opening and managing connection
    public static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    public void init() {
        String query = "select email from shadows";

        try {
            con = DriverManager.getConnection(url, user, password);

            stmt = con.createStatement();

            rs = stmt.executeQuery(query);

            while (rs.next()) {
                //System.out.println("Total number of books in the table : " + rs.getNString(1));
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

}

