package gui.Forms.reglib;

import database.BaseConnection;
import database.RoutineWrappers;
import gui.manager.LibraryGuiManager;

import javax.swing.*;
import java.awt.event.*;
import java.sql.SQLException;

public class NewReader extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel label;
    private JTextField id;
    private LibraryGuiManager lgm;

    public NewReader(LibraryGuiManager libraryGuiManager) {
        lgm = libraryGuiManager;
      //  contentPane.add(new JLabel("Введите номер читательского билета"));
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        try {
            RoutineWrappers.createNewReader(BaseConnection.con, id.getText());
        }
        catch (SQLException e){e.printStackTrace();}
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        NewReader dialog = new NewReader(null);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
