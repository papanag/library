package gui.Forms.reader;

import database.BaseConnection;
import database.Book;
import database.Order;
import database.RoutineWrappers;
import gui.manager.LibInitHelper;
import gui.manager.LibraryGuiManager;

import javax.swing.*;
import java.awt.*;
import java.sql.ResultSet;

public class OrderCreate extends JFrame {
    private JButton EButton;
    private JScrollPane ordersField;
    private JPanel panel1;

    public JPanel getPanelMain() {
        return panelMain;
    }

    private JPanel panelMain;
    private JButton OrdButton;
    private String role;

    public OrderCreate(){
        this.role = role;
        setContentPane(panelMain);
        panel1.add(new JLabel("Навзание     Автор        МежБиблиотечныйАб?        МаксКоличество"));

        panel1.setLayout(new GridLayout(100,1,10,0));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void init(LibraryGuiManager libraryGuiManager, Order order, LibInitHelper libInitHelper) throws Exception{
        panel1.removeAll();
        panel1.add(new JLabel("Столбцы: Название, Автор, Номер пункта Выдачи, Кол-во в наличии"));
        //System.out.println(order.getOut_id() + "hhh");
        ResultSet res;
       // System.out.println(RoutineWrappers.GetOutType(order.getOut_id()));
        if (RoutineWrappers.GetOutType(order.getOut_id()).equals("ABONEMENT"))
            res = RoutineWrappers.GetAllBooks(BaseConnection.con);
        else res = RoutineWrappers.GetOutBooks(order.getOut_id());

        OrdButton.addActionListener((a)->{
            try{
                int m_c = RoutineWrappers.maxBooksPer(order.getOut_id());
                if (m_c < order.allCount() && m_c != -1){
                    LibInitHelper.ErrorWithText("Уберите лишние книги! Можно : "+
                            RoutineWrappers.maxBooksPer(order.getOut_id()) + " was: " +order.allCount());
                    return;
                }
            }
            catch (Exception e){ return;}
            order.createOrder(role);
        });

        EButton.addActionListener((a)->{
            libraryGuiManager.stage = "regreader";
            libInitHelper.switchPanel();
            //order.createOrder();
        });

        while (res.next()){
            OrderOne orderOne = new OrderOne();
            orderOne.setId(res.getInt(6));
            int p = res.getInt(3);
            orderOne.getTakeButton().addActionListener((a)->{
                try {
                    //System.out.println(orderOne.getId() + " " + orderOne.getTextField1().getText());
                    orderOne.getDeleteButton().setVisible(true);
                    orderOne.getTakeButton().setVisible(false);
                    order.getBooks().put(new Book(orderOne.getId(), p), Integer.parseInt(orderOne.getTextField1().getText()));
                    order.getBooks().forEach((x,y)->{
                                //System.out.println(x.id + " " + x.post_where + " " + y);
                            }
                    );
                }
                catch (Exception ignored){ignored.printStackTrace();}
            });

            orderOne.getDeleteButton().addActionListener((e)->{
                orderOne.getDeleteButton().setVisible(false);
                orderOne.getTakeButton().setVisible(true);
                order.getBooks().remove(new Book(orderOne.getId(), p));
                orderOne.getTextField1().setText("0");
                order.getBooks().forEach((x,y)->{
                            //System.out.println(x.id + " " + x.post_where + " " + y);
                        }
                );
            });




            String s =  res.getString(2) + "          " +
                    res.getString(1) + "       " +
                    res.getInt(3) + "        " +
                    res.getInt(4) + "        ";
           // System.out.println(s);
            orderOne.setText(s);
            // if (oc.getPanelMain() == null) System.out.println("gg");
            panel1.add(orderOne.panel1);
            //res.close();
        }

    }

    public void setRole(String role) {
        this.role = role;
    }

    public static void main(String[] args) {
        OrderCreate orderCreate = new OrderCreate();
    }
}
