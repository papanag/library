import database.BaseConnection;
import gui.manager.LibraryGuiManager;

import java.util.Locale;

public class LibClient {
    public static void main(String args[]) {
        Locale.setDefault(Locale.ENGLISH);
        BaseConnection.password = args[1];
        BaseConnection.user = args[0];

        BaseConnection bc = new BaseConnection();
        bc.init();
        LibraryGuiManager lgm = new LibraryGuiManager();
        lgm.start();
    }
}