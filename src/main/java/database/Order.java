package database;

import gui.manager.LibInitHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.lang.Math.abs;

public class Order {
    int id;
    int max = 0;
    int out_id = 0;
    String email;

    public int getOut_id() {
        return out_id;
    }

    public void setOut_id(int out_id) {
        this.out_id = out_id;
    }


    public void setRead_id(String read_id) {
        this.email = read_id;
    }


    HashMap<Book, Integer> books = new HashMap<>();

    public HashMap<Book, Integer> getBooks() {
        return books;
    }

    public int allCount(){
        Integer sum = 0;
       for( Integer i: books.values()){
           sum += i;
       }
       return sum;
    }

    public void createOrder(String role) {
        id = abs(UUID.randomUUID().hashCode());
        try {
            if (!RoutineWrappers.isOrderable(email, role)){
                LibInitHelper.ErrorWithText("Вы не можете делать заказы: у вас их слишком много");
                return;
            }

        }
        catch (Exception ignored){ignored.printStackTrace();}
        boolean isSuccess = true;
        for (Map.Entry<Book, Integer> entry : books.entrySet()) {
            int count  = entry.getValue();
            Book book = entry.getKey();
            try {
                int res;
                if (count > 0){
                    res = RoutineWrappers.CreatePartOrder(BaseConnection.con, email, count, book.post_where, book.id, out_id, id);
                    //System.out.println(res);
                    if (res == -101) {
                        LibInitHelper.ErrorWithText("Вы не можете делать заказы: У вас Ограничение на пользование");
                        isSuccess = false;
                        return;
                    }
                    if (res == -100) {
                        LibInitHelper.ErrorWithText("Вы не можете делать заказы: Вы можете только брать для чтение в зале");
                        isSuccess = false;
                        return;
                    }
                    //System.out.println(res);;
                }

                else {
                    LibInitHelper.ErrorWithText("Вы не можете делать заказы: у вас их слишком много");
                    isSuccess = false;
                    return;
                }
            }
            catch (Exception ignored){ignored.printStackTrace();}
        }

        if (isSuccess) {
            LibInitHelper.ErrorWithText("Успешно");
            books.clear();
        }




    }
}
