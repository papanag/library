package database;

import database.roles.LibraryWorker;
import oracle.jdbc.internal.OracleTypes;
import oracle.jdbc.oracore.OracleType;

import java.lang.reflect.Type;
import java.sql.*;
import java.util.UUID;

import static java.lang.Math.abs;

public class RoutineWrappers {

    public static boolean isLoginSuccess(Connection con, String log, String pas) throws SQLException {
        CallableStatement cstmt = con.prepareCall("{? = call LibraryAuth(?, ?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, log);
        cstmt.setString(3, pas);
        cstmt.execute();
        return cstmt.getInt(1) == 0;
    }


    public static String tryLogin(Connection con, String log, String pas) throws SQLException {
        if (!isLoginSuccess(con, log, pas)) throw new SQLException();
        CallableStatement cstmt = con.prepareCall("{? = call GetRole(?)}");
        cstmt.registerOutParameter(1, Types.VARCHAR);
        cstmt.setString(2, log);
        cstmt.execute();
        //System.out.println(cstmt.getString(1).toLowerCase());
        return cstmt.getString(1).toLowerCase();
    }

    public static int createNewReader(Connection con, String email) throws SQLException {
        UUID idOne = UUID.randomUUID();
        CallableStatement cstmt = con.prepareCall("{? = call createNewReader(?, ?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setString(2, email);
        cstmt.setInt(3,  abs(idOne.hashCode()));
        cstmt.execute();
        return cstmt.getInt(1);
    }

    public static LibraryWorker GetLibInfo(Connection con, String email) throws SQLException{
        CallableStatement cstmt = con.prepareCall("{? = call GetLibraryWorkerInfo(?)}");
        cstmt.registerOutParameter(1, OracleTypes.STRUCT, "LIBINFO");
        cstmt.setString(2, email);
        cstmt.execute();
        Struct emp = (Struct)cstmt.getObject(1);
        Object[] attrEmp = emp.getAttributes();
        return new LibraryWorker(attrEmp[0].toString(), Integer.parseInt(attrEmp[1].toString()));

    }

    public static int ExtendCard(Connection con, int card, int post) throws SQLException{
        CallableStatement cstmt = con.prepareCall("{? = call ExtendCard(?, ?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setInt(2, card);
        cstmt.setInt(3, post);
        cstmt.execute();
        return cstmt.getInt(1);
    }

    public static int CreatePartOrder(Connection con, String email,  int count, int post_from, int b_id, int post_to,  int id) throws SQLException{
        ResultSet res = ReaderCard(email);
        res.next();
        //System.out.println(res.getInt(1) + " " + count + " " + post_from + " " + b_id);
        CallableStatement cstmt = con.prepareCall("{? = call CreatePartOrder(?, ?, ?,?, ?, ?)}");
        cstmt.registerOutParameter(1, Types.INTEGER);
        cstmt.setInt(2, b_id);
        cstmt.setInt(3, count);
        cstmt.setInt(4, post_from);
        cstmt.setInt(5, res.getInt(1));
        cstmt.setInt(6, id);
        cstmt.setInt(7, post_to);
        cstmt.execute();
        return cstmt.getInt(1);
    }

    public static ResultSet GetAllBooks(Connection connection) throws SQLException{
        Statement statement = connection.createStatement();
        return statement.executeQuery("SELECT NAME, AUTHOR, ID_OUT, COUNT, MAX_COUNT_FOR_PERSON, ID_BOOK " +
                "FROM OUTPOSTS INNER JOIN (BOOKS INNER JOIN BOOKS_OUT BO on BOOKS.ID = BO.ID_BOOK) on OUTPOSTS.ID = ID_OUT");
    }

    public static ResultSet GetOutBooks(int out) throws SQLException{
        PreparedStatement statement = BaseConnection.con.prepareStatement("SELECT NAME, AUTHOR, ID_OUT, COUNT, MAX_COUNT_FOR_PERSON, ID_BOOK " +
                "FROM OUTPOSTS INNER JOIN (BOOKS INNER JOIN BOOKS_OUT BO on BOOKS.ID = BO.ID_BOOK) on OUTPOSTS.ID = ID_OUT WHERE ID_OUT = (?)");
        statement.setInt(1, out);
        return statement.executeQuery();
    }

    public static String GetOutType(int out) throws SQLException{
        PreparedStatement statement = BaseConnection.con.prepareStatement("SELECT TYPE FROM OUTPOSTS WHERE ID = (?)");
        statement.setInt(1, out);
        ResultSet res = statement.executeQuery();
        res.next();
        return res.getString(1);
    }


    public static ResultSet GetAllOrders(String email) throws SQLException{
        ResultSet resultSet = ReaderCard(email);
        resultSet.next();
        int card = resultSet.getInt(1);
        PreparedStatement statement = BaseConnection.con.prepareStatement("SELECT ID_ORD, NAME, COUNT, ID_OUT, STATUS FROM" +
                " ORDERS INNER JOIN BOOKS ON BOOKS.ID = ORDERS.ID_BOOK WHERE READER_ID = (?)");
        statement.setInt(1, card);
        return statement.executeQuery();
    }

    public static ResultSet GetAllOrders_D(String email) throws SQLException{
        ResultSet resultSet = ReaderCard(email);
        resultSet.next();
        int card = resultSet.getInt(1);
        PreparedStatement statement = BaseConnection.con.prepareStatement("SELECT DISTINCT ID_ORD FROM" +
                " ORDERS INNER JOIN BOOKS ON BOOKS.ID = ORDERS.ID_BOOK WHERE READER_ID = (?)");
        statement.setInt(1, card);
        return statement.executeQuery();
    }

    public static ResultSet ReaderCard(String email) throws SQLException{
        PreparedStatement statement = BaseConnection.con.prepareStatement("SELECT READER_ID FROM LIBRARY_READERS WHERE  NSU_EMAIL = (?)");
        statement.setString(1, email);
        return statement.executeQuery();
    }

    public static void isHaveRegistration(String email, int id) throws SQLException{
        ResultSet resultSet = ReaderCard(email);
        resultSet.next();
        int card = resultSet.getInt(1);
        PreparedStatement statement = BaseConnection.con.prepareStatement("SELECT count(*) FROM READER_REGISTRATION WHERE  READER_ID = (?) " +
                "AND DATE_END > SYSDATE AND OUT_POST_ID = (?)");
        statement.setInt(1, card);
        statement.setInt(2, id);
        //System.out.println(card + " " + id);
        ResultSet res = statement.executeQuery();
        res.next();
        //System.out.println(res.getInt(1));
        if  (res.getInt(1) == 0) throw new SQLException();
    }

    public static int maxBooksPer(int id) throws SQLException{
        PreparedStatement statement = BaseConnection.con.prepareStatement("SELECT MAX_COUNT_FOR_PERSON FROM OUTPOSTS WHERE ID = (?)");
        statement.setInt(1, id);
        ResultSet res = statement.executeQuery();
        res.next();
        return res.getInt(1);
    }

    public static boolean isOrderable(String email, String role) throws SQLException{

        if (role.equals("notregreader")) return false;

        ResultSet res = GetAllOrders_D(email);
        int i = 0 ;
        while (res.next()){
            if (!res.getString(1).equals("РАЗРЕШЕН"))
                i++;
        }
        //System.out.println("i = " + i);
        res.close();
        if ( i > 3) return false;
        return true;
    }

    public static ResultSet getOrder(int ord) throws SQLException{
        PreparedStatement statement = BaseConnection.con.prepareStatement("SELECT ID_BOOK, COUNT, STATUS, ID_OUT, NAME, OUT_FROM_TAKE" +
                " FROM ORDERS O, BOOKS B " +
                "WHERE O.ID_ORD = (?) AND B.ID = O.ID_BOOK");
        statement.setInt(1, ord);
        return statement.executeQuery();
    }


    public static int takeBook(int ord) {
        try {
            CallableStatement cstmt = BaseConnection.con.prepareCall("{? = call TakeBooks(?)}");
            cstmt.registerOutParameter(1, Types.INTEGER);
            cstmt.setInt(2, ord);
           // System.out.println(ord);
            cstmt.execute();
            return cstmt.getInt(1);
        }
        catch (Exception e){
            e.printStackTrace();
            return -1;}

    }

    public static int backBook(int ord, int book, int count, int out) {
        try {
            CallableStatement cstmt = BaseConnection.con.prepareCall("{? = call BackSomeBooks(?, ?, ?, ?)}");
            cstmt.registerOutParameter(1, Types.INTEGER);
            cstmt.setInt(2, book);
            cstmt.setInt(3, count);
            cstmt.setInt(4, ord);
            cstmt.setInt(5, out);
            cstmt.execute();
            return cstmt.getInt(1);
        }
        catch (Exception e){
            e.printStackTrace();
            return -1;}

    }

}
