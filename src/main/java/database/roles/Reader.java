package database.roles;

import database.Order;

import java.util.Vector;

public class Reader {
    public int getReaderCard() {
        return readerCard;
    }

    public void setReaderCard(int readerCard) {
        this.readerCard = readerCard;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Vector<Order> getOrders() {
        return orders;
    }

    public void setOrders(Vector<Order> orders) {
        this.orders = orders;
    }

    int readerCard;
    String info;
    Vector<Order> orders = new Vector<>();
    Vector<Integer> reg = new Vector<>();
}
