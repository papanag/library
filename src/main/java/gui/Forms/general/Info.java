package gui.Forms.general;

import javax.swing.*;

public class Info extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel label;
    private JButton buttonCancel;

    public Info() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        buttonOK.addActionListener((e)->dispose());
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    }

    public void setText(String text){
        label.setText(text);
    }

    public JButton getButtonOK() {
        return buttonOK;
    }

    public static void main(String[] args) {
        Info dialog = new Info();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
