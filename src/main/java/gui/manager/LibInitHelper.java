package gui.manager;

import database.*;
import gui.Forms.general.Info;
import gui.Forms.general.LoginForm;
import gui.Forms.general.SampleIn;
import gui.Forms.reader.ReaderForm;
import gui.Forms.reglib.ContTicket;
import gui.Forms.reglib.NewReader;
import gui.Forms.reglib.OrdersLibWorker;
import gui.Forms.reglib.RegLibMainForm;


import javax.swing.*;
import java.sql.SQLException;

public class LibInitHelper {
    private Order order ;
    private ManagerFrames managerFrames;
    private LibraryGuiManager libraryGuiManager;

    public static void ErrorWithText(String text){
        Info info = new Info();
        info.setText(text);
        info.pack();
        info.setVisible(true);
    }

    public LibInitHelper(ManagerFrames mf, LibraryGuiManager lgm ){
        managerFrames = mf;
        libraryGuiManager = lgm;
    }


    public void initLoginButton(LoginForm loginForm){
        loginForm.inB.addActionListener((e)->{
            String login = loginForm.getLogin();
            String pass = loginForm.getPass();
            try {
                libraryGuiManager.stage = RoutineWrappers.tryLogin(BaseConnection.con,login, pass);
                libraryGuiManager.role = Roles.valueOf(libraryGuiManager.stage.toUpperCase());

                libraryGuiManager.getOc().setRole(libraryGuiManager.role.getOb());

                System.out.println(libraryGuiManager.role);
                libraryGuiManager.login = login;
                if (libraryGuiManager.stage.equals("reglib")) libraryGuiManager.afterLoginInit();
            }
            catch (SQLException s){
                ErrorWithText("Информация неверна!");
                return;
            }
            catch (IllegalArgumentException s){};
            switchPanel();

        });
    }

    public void initRegLibForm(RegLibMainForm rlmf){
        initNewReaderButtom(rlmf, libraryGuiManager.nr);
        initContCard(rlmf.getContBilB(), libraryGuiManager.ct);
        //initChangeOrder(rlmf.getWatchOrders());
        initExit(rlmf.getExitB());
    }

    private void initNewReaderButtom(RegLibMainForm rlmf, NewReader reader){
        rlmf.getNewReaderB().addActionListener((e)->{
            reader.pack();
            reader.setVisible(true);
        });
    }

    public void initChangeOrder(JButton b, OrdersLibWorker ordersLibWorker, int ord){

        SampleIn sampleIn = new SampleIn();
        sampleIn.setLabelText("Введите номер заказа");
        sampleIn.getButtonOK().addActionListener((s)->{
            ordersLibWorker.init(Integer.parseInt(sampleIn.getTextField1().getText()), ord);
            libraryGuiManager.stage = "changeorder";
            switchPanel();
            sampleIn.dispose();
        });

        b.addActionListener((e)->{
            sampleIn.pack();
            sampleIn.setVisible(true);
        });
    }

    private void initExit(JButton button){
        button.addActionListener((e)->{
            libraryGuiManager.stage = "login";
            switchPanel();
        });
    }

    private void initContCard(JButton button, ContTicket contTicket){
        button.addActionListener((e)->{
            contTicket.pack();
            contTicket.setVisible(true);
        });
    }

    public void initReaderForm(ReaderForm readerForm)
    {
        initOrdersButtom(readerForm);
        initViewButtom(readerForm);
        initExit(readerForm.getExitB());
    }

    private void initOrdersButtom(ReaderForm readerForm){
        order = new Order();
        //выбор места выдачи - инициализация кнопок и логика при отсвуствии регистрации
        SampleIn sampleIn = new SampleIn();
        sampleIn.getButtonOK().addActionListener((a)->{
            order.setRead_id(libraryGuiManager.login);
            try{
              RoutineWrappers.isHaveRegistration(libraryGuiManager.login, Integer.parseInt(sampleIn.getTextField1().getText()));
            }
            catch (Exception e){
               ErrorWithText("Отказано! Нет регистрации в данном пункте выдачи");
                return;
            }
            order.setOut_id(Integer.parseInt(sampleIn.getTextField1().getText()));
            libraryGuiManager.stage = "createorder";
            switchPanel();
            sampleIn.dispose();
        });

        sampleIn.setLabelText("Введите, где хотите получить заказ");
        readerForm.getCreate_order().addActionListener((e)->{
            sampleIn.pack();
            sampleIn.setVisible(true);
        });
    }

    private void initViewButtom(ReaderForm readerForm){
        libraryGuiManager.ov.getButton1().addActionListener((e)->{
            libraryGuiManager.stage = "regreader";
            switchPanel();
        });
        readerForm.getViewOrder().addActionListener((e)->{
            try {
                libraryGuiManager.getOv().init(libraryGuiManager);
            }
            catch (Exception r){r.printStackTrace();}

            libraryGuiManager.stage = "vieworder";
            switchPanel();
        });
    }

    public void initOrdersLibWorker(OrdersLibWorker ordersLibWorker){
        initEOrB(ordersLibWorker.geteBut());
    }
    private void initEOrB(JButton ex){
        ex.addActionListener((e)->{
            libraryGuiManager.stage = "reglib";
            switchPanel();
        });
    }
    public void switchPanel(){
        switch (libraryGuiManager.stage){
            case "reglib" :
                String outpost = "нет места выдачи";
                try {
                    outpost = RoutineWrappers.GetLibInfo(BaseConnection.con, libraryGuiManager.login).getOutpost_id().toString();
                }
                catch (Exception ignored){}

                libraryGuiManager.rlmf.setMainLabel("Вы вошли как: " + libraryGuiManager.lf.getLogin());
                libraryGuiManager.rlmf.setrole("Ваша роль: " + libraryGuiManager.role.getOb());
                libraryGuiManager.rlmf.setOutpost("Ваша точка выдачи: " + outpost);
                break;
            case "regreader" :
                libraryGuiManager.rf.setHello("Вы вошли как: " + libraryGuiManager.lf.getLogin());
                libraryGuiManager.rf.setrole("Ваша роль: " + libraryGuiManager.role.getOb() );
                break;
            case "createorder" :
                try {
                    addBooks();
                }
                catch (Exception ignored){}
                break;
            case "vieworder" :
                //System.out.println("ggg4");
                break;
            default:
                System.out.println(libraryGuiManager.role.getOb());
        }
        managerFrames.changePanel(libraryGuiManager.stage);

    }

    private void addBooks() throws Exception{
        libraryGuiManager.getOc().init(libraryGuiManager,order,this);

    }
}
