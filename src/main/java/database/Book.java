package database;

import java.util.Objects;

public class Book {
   public int id;
   public int post_where;

    public Book(int id, int post_where) {
        this.id = id;
        this.post_where = post_where;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id &&
                post_where == book.post_where;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, post_where);
    }
}
