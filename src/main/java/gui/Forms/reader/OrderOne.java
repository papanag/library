package gui.Forms.reader;

import javax.swing.*;
import java.awt.event.ActionListener;

public class OrderOne {
    public JPanel panel1;
    private JTextField textField1;
    private JButton takeButton;
    private JLabel mainLabel;
    private JButton DeleteButton;
    private Integer id;

    public OrderOne(){
        DeleteButton.setVisible(false);
    }
    public JButton getDeleteButton() {
        return DeleteButton;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setText(String text){
        mainLabel.setText(text);
    }

    public JButton getTakeButton() {
        return takeButton;
    }

    public JTextField getTextField1() {
        return textField1;
    }
}
