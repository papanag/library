package gui.Forms.reglib;

import database.BaseConnection;
import database.RoutineWrappers;
import gui.manager.LibraryGuiManager;

import javax.swing.*;
import java.awt.event.*;

public class ContTicket extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textField1;
    private JPanel hello;
    private JLabel hell;
    LibraryGuiManager lgm;
    public ContTicket( LibraryGuiManager libraryGuiManager) {
        lgm = libraryGuiManager;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        try {
            int outpost = RoutineWrappers.GetLibInfo(BaseConnection.con, lgm.login).getOutpost_id();
            RoutineWrappers.ExtendCard(BaseConnection.con, Integer.parseInt(textField1.getText()), outpost);
        }
        catch (Exception ignored){}
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        ContTicket dialog = new ContTicket(null);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
