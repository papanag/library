package gui.Forms.reglib;

import database.Order;
import database.RoutineWrappers;
import gui.manager.LibInitHelper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class OrdersLibWorker {
    private Vector<OrderWatchOne> orderWatchOneVector = new Vector<>();
    private JPanel mainPanel;

    public JButton geteBut() {
        return eBut;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    private JButton eBut;
    private JButton mainB;
    private JPanel panel1;
    private int id_ord;
    private String status = "undefined";
    private boolean isInit  = false;

    public void init(int ord, int thisOut){
        System.out.println(ord + " " + thisOut);
        orderWatchOneVector.clear();
        id_ord = ord;
        panel1.removeAll();
        panel1.setLayout(new GridLayout(100,1,10,0));
        panel1.add(new JLabel("СТолбцы ID_BOOK, COUNT, STATUS, ID_OUT, NAME"));

        try{
            ResultSet res = RoutineWrappers.getOrder(ord);

            while (res.next()){
                status = res.getString(3);
                if (res.getInt(4) != thisOut) {
                    LibInitHelper.ErrorWithText("Это не ваш заказ!");
                    return;
                 }
                OrderWatchOne watchOne = new OrderWatchOne();
                watchOne.setBook_id(res.getInt(1));
                watchOne.setOut(res.getInt(6));

                orderWatchOneVector.add(watchOne);
                watchOne.setText(res.getString(5) + "        "
                        + Integer.toString(res.getInt(1)) + "        "
                        + Integer.toString(res.getInt(2)) + "        "
                        + res.getInt(4) + "            "
                        + res.getString(3));
                panel1.add(watchOne.mainPanel);
            }
            if (status.equals("ОЖИДАЕТСЯ ВЫДАЧА")){
                for( ActionListener al : mainB.getActionListeners() ) {
                    mainB.removeActionListener( al );
                }
                mainB.addActionListener((s)-> System.out.println(RoutineWrappers.takeBook(ord)));
            }
            if (status.equals("НА РУКАХ")){
                for( ActionListener al : mainB.getActionListeners() ) {
                    mainB.removeActionListener( al );
                }
                initPutB(ord);
            }

        }
        catch (SQLException e){e.printStackTrace();}
    }

    private void initPutB(int ord){

        mainB.addActionListener((e)->{
            System.out.println(orderWatchOneVector.size());
            for (OrderWatchOne orderWatchOne : orderWatchOneVector){
                int a = Integer.parseInt(orderWatchOne.getTextField1().getText());
                int b = orderWatchOne.getOut();
                System.out.println(ord+ " " +  orderWatchOne.book_id + " " +  a + " " + b);
                   int h =  RoutineWrappers.backBook(ord, orderWatchOne.book_id, a, b);
                System.out.println("h = " + h);
            }

        });
    }
}
