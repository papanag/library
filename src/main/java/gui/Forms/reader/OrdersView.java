package gui.Forms.reader;

import database.RoutineWrappers;
import gui.manager.LibraryGuiManager;

import javax.swing.*;
import java.awt.*;
import java.sql.ResultSet;

public class OrdersView extends JFrame {
    private JButton button1;
    private JPanel mainPanel;
    private JScrollPane scrollPanel;
    private JPanel panel1;

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public OrdersView(){
        setContentPane(mainPanel);
        panel1.setLayout(new GridLayout(100,1,10,0));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public JButton getButton1() {
        return button1;
    }

    public void init(LibraryGuiManager libraryGuiManager) throws Exception{
        ResultSet res = RoutineWrappers.GetAllOrders(libraryGuiManager.login);
        panel1.add(new JLabel("Столбцы: Номер заказа, название, кол-во, где получать, статус"));
        while (res.next()){
            ViewOneOrder viewOneOrder = new ViewOneOrder();
            viewOneOrder.getLabel().setText(res.getInt(1) + "        "
                    +  res.getString(2)+ "      "
                    + res.getInt(3)+ "      "
                    + res.getInt(4) + "    "
                    +  res.getString(5));
            panel1.add(viewOneOrder.getMain());
        }
    }


}


