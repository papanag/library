package database;

public enum Roles {
    REGLIB("reglib", "Штатный сотрудник"),
    UNDEFINED( "undefined", "Неопознанная роль"),
    REGREADER( "regreader", "Постоянный читатель");
    private String name;
    private String ob;
    Roles(String name, String ob){
        this.name = name;
        this.ob = ob;
    }
    public Roles find(String name){
        return  Roles.valueOf(name);
    }

    public String getOb() {
        return ob;
    }
}
