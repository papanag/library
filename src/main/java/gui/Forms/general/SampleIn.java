package gui.Forms.general;

import javax.swing.*;
import java.awt.event.*;

public class SampleIn extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField textField1;
    private JLabel label;

    public SampleIn() {
        setContentPane(contentPane);
        setModal(true);
    }

    public void setLabelText(String text){
        label.setText(text);
    }

    public JTextField getTextField1() {
        return textField1;
    }


    public JButton getButtonOK() {
        return buttonOK;
    }

    public JButton getButtonCancel() {
        return buttonCancel;
    }
}
