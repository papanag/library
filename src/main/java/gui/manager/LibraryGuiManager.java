package gui.manager;

import database.BaseConnection;
import database.Roles;
import database.RoutineWrappers;
import gui.Forms.general.LoginForm;
import gui.Forms.reader.OrderCreate;
import gui.Forms.reader.OrdersView;
import gui.Forms.reader.ReaderForm;
import gui.Forms.reglib.ContTicket;
import gui.Forms.reglib.NewReader;
import gui.Forms.reglib.OrdersLibWorker;
import gui.Forms.reglib.RegLibMainForm;


import java.awt.*;

public class LibraryGuiManager {

    public String login;
    private boolean isInit = false;
    public Roles role = Roles.UNDEFINED;
    public String stage = "login";
    private ManagerFrames managerFrames;
    LibInitHelper lsm;

    OrderCreate oc = new OrderCreate();
    LoginForm lf = new LoginForm();
    RegLibMainForm rlmf = new RegLibMainForm();
    ReaderForm rf = new ReaderForm();
    OrdersLibWorker olw = new OrdersLibWorker();

    public OrdersView getOv() {
        return ov;
    }

    OrdersView ov = new OrdersView();

    public ContTicket ct = new ContTicket(this);
    public NewReader nr = new NewReader(this);

    public LibraryGuiManager(){
        managerFrames = new ManagerFrames();
        lsm = new LibInitHelper( managerFrames, this);

        initAllFrames();
    }

    public OrderCreate getOc() {
        return oc;
    }

    public void setOc(OrderCreate oc) {
        this.oc = oc;
    }

    private void initAllFrames(){
        lsm.initLoginButton(lf);
        lf.mainPanel.setSize(new Dimension(380, 300));
        managerFrames.addJpanel(lf.mainPanel, "login");

        lsm.initRegLibForm(rlmf);

        rlmf.getPanel1().setSize(new Dimension(500, 400));
        managerFrames.addJpanel(rlmf.getPanel1(), "reglib");

        rf.getPanel().setSize(new Dimension(600, 400));
        lsm.initReaderForm(rf);
        managerFrames.addJpanel(rf.getPanel(), "regreader");

        oc.getPanelMain().setSize(new Dimension(1000, 400));
        managerFrames.addJpanel(oc.getPanelMain(), "createorder");

        ov.getMainPanel().setSize(new Dimension(1000, 400));
        managerFrames.addJpanel(ov.getMainPanel(), "vieworder");

        olw.getMainPanel().setSize(new Dimension(1000, 400));
        lsm.initOrdersLibWorker(olw);
        managerFrames.addJpanel(olw.getMainPanel(), "changeorder");




    }

    public void afterLoginInit(){
        if (isInit) return;
        isInit = true;
        try {
            lsm.initChangeOrder(rlmf.getWatchOrders(), olw, RoutineWrappers.GetLibInfo(BaseConnection.con, login).getOutpost_id());
        }
        catch (Exception e){e.printStackTrace();}
    }

    public void start(){
        managerFrames.changePanel("login");
    }
}
