package gui.manager;

import database.BaseConnection;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class ManagerFrames {
    HashMap<String, JPanel> panels;
    JFrame mainFrame = new JFrame();

   public ManagerFrames(){
       panels = new HashMap<>();

       mainFrame.setVisible(true);
       mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       mainFrame.setSize(new Dimension(380, 300));
   }
   public void addJpanel(JPanel panel, String s){
       panels.put(s, panel);
   }

   public void changePanel(String key){
       if (panels.containsKey(key)) {
           mainFrame.setSize(panels.get(key).getSize());
           mainFrame.setContentPane(panels.get(key));
           mainFrame.validate();
       }
   }
}
